#ifndef _UART_
#define _UART

int UART_init();
int UART_write(int uartFilestream, char *txBuffer, char *pTxBuffer);
int UART_read(int uartFilestream, char *rx_buffer);
void UART_close();
int UART_cmd(char cmd[]);

#endif