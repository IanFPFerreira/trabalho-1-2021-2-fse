# Trabalho 1 - 2021-2 - FSE

Trabalho 1 da matéria de Fundamentos de Sistemas Embarcados que tem por objetivo a implementação de um sistema que simula o controle de um forno para soldagem de placas de circuito impresso (PCBs).

### Como rodar

1. Clonar o repositório

2. Transferir toda a pasta do repositório para o servidor SSH da RASP.

3. Acessar o servidor SSH da RASP

4. Na pasta raiz do projeto no servidor SSH, executar o comando:

```
make run_raspbian
```

### Autor

**Ian Fillipe Pontes Ferreira**