#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
// #include <wiringPiI2C.h>
// #include <wiringPi.h>

#include "../inc/uart.h"
#include "../inc/i2clcd.h"

char cmd_user[3] = {0x01, 0x23, 0xC1};
char system_on_off[3] = {0x01, 0x16, 0xD3};


void ligar_forno(){
    if (UART_cmd(system_on_off)){
        printf("Forno ligado!\n");
    }
    else{
        printf("Erro ao ligar forno :(\n");
    }
}


void desligar_forno(){
    if (UART_cmd(system_on_off)){
        printf("Forno desligado!\n");
    }
    else{
        printf("Erro ao desligar forno :(\n");
    }
}


int main(){

    UART_init();

    // signal(SIGINT, desligar);

    while(1){

        int cmd = UART_cmd(cmd_user);

        printf("1 - Ligar forno\n");
        printf("2 - Desligar forno\n");
        printf("0 - Fechar programa\n");
        printf("Comando selecionado: %d\n", cmd);

        if (cmd == 0){
            break;
        }
        if (cmd == 1){
            ligar_forno();
        }
        if (cmd == 2){
            desligar_forno();
        }


    }

    UART_close();
    return 0;
}