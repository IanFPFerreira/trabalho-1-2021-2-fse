#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> // Used for UART
#include <fcntl.h> // Used for UART
#include <termios.h> // Used for UART
#include "../inc/uart.h"
#include "../inc/crc16.h"

static int uart0_filestream = -1;

int UART_init(){
    uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);      //Open in non blocking read/write mode
    if (uart0_filestream == -1)
    {
        printf("Erro - Não foi possível iniciar a UART.\n");
        return -1;
    }
    else
    {
        printf("UART inicializada!\n"); 
    }    
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;// <Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);

    return uart0_filestream;
}

int UART_write(int uart0_filestream, char *txBuffer, char *pTxBuffer){
    if (uart0_filestream != -1)
    {
        printf("Escrevendo caracteres na UART ...");
        int count = write(uart0_filestream, txBuffer, (pTxBuffer - txBuffer));
        if (count < 0)
        {
            printf("UART TX error\n");
            return -1;
        }
        else
        {
            printf("escrito.\n");
            return 1;
        }
    }
    else
        return -1;
}

int UART_read(int uart0_filestream, char *rx_buffer){
    if (uart0_filestream != -1)
    {
        // Read up to 255 characters from the port if they are there
        // unsigned char *rx_buffer = malloc(256);
        int rx_length = read(uart0_filestream, (void*)rx_buffer, 255); //Filestream, buffer to store in, number of bytes to read (max)
        if (rx_length < 0)
        {
            printf("Erro na leitura.\n"); //An error occured (will occur if there are no bytes)
            return 0;
        }
        else if (rx_length == 0)
        {
            printf("Nenhum dado disponível.\n"); //No data waiting
            return 0;
        }
        else
        {
            //Bytes received
            rx_buffer[rx_length] = '\0';
            return rx_length;            
        }
    }
    else
    {
        printf("Erro - Problema na UART.\n");
        return 0;
    }
}

void UART_close(){
    close(uart0_filestream);
}



int UART_cmd(char cmd[]){

    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;

    p_tx_buffer = &tx_buffer[0];
    *p_tx_buffer++ = cmd[0];
    *p_tx_buffer++ = cmd[1];
    *p_tx_buffer++ = cmd[2];

    *p_tx_buffer++ = 2;
    *p_tx_buffer++ = 0;
    *p_tx_buffer++ = 8;
    *p_tx_buffer++ = 7;

    short crc = calculate_CRC(&tx_buffer[0], 7);
    *p_tx_buffer++ = crc & 0xFF;
    *p_tx_buffer++ = (crc >> 8) & 0xFF;

    if (uart0_filestream != -1)
    {
        int rx_length = read(uart0_filestream, (void*)tx_buffer, 255); //Filestream, buffer to store in, number of bytes to read (max)
        if (rx_length < 0)
        {
            printf("Erro na leitura.\n"); //An error occured (will occur if there are no bytes)
            return 0;
        }
        else if (rx_length == 0)
        {
            printf("Nenhum dado disponível.\n"); //No data waiting
            return 0;
        }
        else
        {
            return tx_buffer[3] + (tx_buffer[4] << 8) + (tx_buffer[5] << 16) + (tx_buffer[6] << 24);           
        }
    }
}
