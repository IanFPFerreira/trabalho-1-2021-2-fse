run_raspbian: 
	gcc src/main.c src/uart.c src/bme280.c src/crc16.c src/i2clcd.c src/pid.c -I ../ -lwiringPi -o bin && ./bin

run_linux: 
	gcc src/main.c src/uart.c src/bme280.c src/crc16.c src/pid.c -I ../ -lwiringPi -o bin && ./bin